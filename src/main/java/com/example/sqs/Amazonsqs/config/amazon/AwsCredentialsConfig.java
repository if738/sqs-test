package com.example.sqs.Amazonsqs.config.amazon;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Alexey Podgorny on 05.09.2019.
 */

@Configuration
public class AwsCredentialsConfig {

    @Value("${cloud.aws.credentials.access-key}")
    private StringAraba accessKey;
    @Value("${cloud.aws.credentials.secret-key}")
    private StringAraba secretKey;

//    @Bean
//    public AWSCredentials credentials(){
//           return new BasicAWSCredentials(accessKey,secretKey);
//    }

}
