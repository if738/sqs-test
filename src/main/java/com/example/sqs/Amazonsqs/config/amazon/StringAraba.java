package com.example.sqs.Amazonsqs.config.amazon;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Alexey Podgorny on 06.09.2019.
 */

@Slf4j
@Component
@RequiredArgsConstructor
public class StringAraba {


    public static void main(String[] args) {
        String ar = "654321الدولار الأمريكي إلى اليورو000,04$ﻰﺘﺣ$000,05ﻰﺘﺣ";
        String s = reverseNumbers(ar);
        System.out.println(s);
    }


    private static String reverseNumbers(String rawString) {
        char[] chars = rawString.toCharArray();
        StringBuilder allString = new StringBuilder();
        StringBuilder tempNumber = new StringBuilder();
        for (int i = 0; i < chars.length; i++) {
            char currentChar = chars[i];
            char nextChar = 0;
            if (i + 1 < chars.length) {
                nextChar = chars[i + 1];
            }
            if (isComma(currentChar) && isDigit(nextChar)) {
                tempNumber.append(currentChar);
            } else {
                if (isDigit(currentChar)) {
                    tempNumber.append(currentChar);
                } else {
                    allString.append(currentChar);
                }
            }
            if (!isDigit(nextChar) && !isComma(nextChar) && !isDollar(nextChar)) {
                tempNumber.reverse();
                allString.append(tempNumber.toString());
                tempNumber.setLength(0);
            }
        }
        return allString.toString();
    }

    private static boolean isDigit(char c) {
        String s = String.valueOf(c);
        String digit = getMatchOneTime("\\d", s);
        return !digit.isEmpty();
    }

    private static boolean isDollar(char c) {
        String s = String.valueOf(c);
        String digit = getMatchOneTime("\\$", s);
        return !digit.isEmpty();
    }

    private static boolean isComma(char c) {
        String s = String.valueOf(c);
        String digit = getMatchOneTime(",", s);
        return !digit.isEmpty();
    }

    private static String getMatchOneTime(String regex, String text) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        String matches = "";
        if (matcher.find()) {
            matches = matcher.group();
        }
        return matches;
    }

}
