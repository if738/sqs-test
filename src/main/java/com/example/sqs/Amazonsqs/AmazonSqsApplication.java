package com.example.sqs.Amazonsqs;

import com.amazonaws.services.sqs.model.Message;
import com.example.sqs.Amazonsqs.service.SQSMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
public class AmazonSqsApplication implements ApplicationRunner {

	public static void main(String[] args) {
		SpringApplication.run(AmazonSqsApplication.class, args);
	}


	@Autowired
	SQSMessageService sqsMessageService;

	@Override
	public void run(ApplicationArguments args) throws Exception {
//		List<Message> messages = sqsMessageService.receiveAllMessagesForTest(10);
//		System.out.println(messages);
	}
}
