package com.example.sqs.Amazonsqs.service.impl;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.MessageAttributeValue;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.example.sqs.Amazonsqs.service.SQSMessageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Alexey Podgorny on 05.09.2019.
 */

@Slf4j
@Component
@RequiredArgsConstructor
public class SQSMessageServiceImpl implements SQSMessageService {

    private final AmazonSQS sqs;
//    private final QueueMessaging Template queueMessagingTemplate;
    @Value("${amazon-sqs.credential.standard-queue-url}")
    private String url;

    @Override
    public void postMessage(String message, Map<String, MessageAttributeValue> messageAttributes) {
        SendMessageRequest sendMessageStandardQueue = new SendMessageRequest()
                .withQueueUrl(url)
                .withMessageBody("A simple message.")
                .withDelaySeconds(30)
                .withMessageAttributes(messageAttributes);
        sqs.sendMessage(sendMessageStandardQueue);
    }

    @Override
    public List<Message> receiveAllMessagesForTest(int maxNumberOfMessages) {
        ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(url);
        List<Message> messages = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            messages.addAll(sqs.receiveMessage(receiveMessageRequest).getMessages());
        }
        return messages;
    }

    @Override
    public void postMessageByStart(String message) {

    }

//    @Override
//    public void postMessageByStart(String message) {
//        this.queueMessagingTemplate.send("testHelloMessage", MessageBuilder.withPayload(message).build());
//    }

    @PostConstruct
    public void test() {
        postMessageByStart("hello");
    }

    @Override
    public void postMessage(String message) {
        SendMessageRequest sendMessageStandardQueue = new SendMessageRequest()
                .withQueueUrl(url)
                .withMessageBody(message)
                .withDelaySeconds(1);
        sqs.sendMessage(sendMessageStandardQueue);
    }


}
