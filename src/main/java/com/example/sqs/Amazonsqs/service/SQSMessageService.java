package com.example.sqs.Amazonsqs.service;

import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.MessageAttributeValue;

import java.util.List;
import java.util.Map;

/**
 * Created by Alexey Podgorny on 05.09.2019.
 */
public interface SQSMessageService{

    void postMessage(String message);
    void postMessage(String message, Map<String, MessageAttributeValue> messageAttributes);
    List<Message> receiveAllMessagesForTest(int maxNumberOfMessages);
    void postMessageByStart(String message);
}
